class AddPasswordDigest < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.index :username
      t.string :password_digest
    end
  end
end
