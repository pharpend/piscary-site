class RemoveFormatColumnsFromPosts < ActiveRecord::Migration
  def change
    remove_column :posts, :format
  end
end
