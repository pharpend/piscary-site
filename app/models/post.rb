require 'redcarpet'
require 'posts_helper'

class Post < ActiveRecord::Base
  attr_accessible :title, :text, :text_html
  attr_accessor :text_html

  include PostsHelper

  # def initialize(attributes={})
  #   attributes[:text_html] = render_html(attributes[:text])
  #   super(attributes)
  # end

  # Title-related
  validates :title,
    presence: true,
    length: { minimum: 1,
              maximum: 150 }

  # Text-related
  validates :text,
    presence: true
end # Post
