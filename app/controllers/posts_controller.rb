require 'redcarpet'
require 'posts_helper'
include PostsHelper

class PostsController < ApplicationController
  def create
  end

  def modify
    @post = Post.find(params[:id])
  end

  def show
    @post = Post.find(params[:id])
    @text_html = render_html(@post.text)
  end
end
