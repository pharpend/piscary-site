require 'spec_helper'

describe "posts/show.html.erb" do
  before do
    # Create a post
    params = {  title: "How I Met Your Mother",
                text: "Paternity *suit*." }
    @my_post = Post.create(params)
    @text_html = render_html(params[:text])

    # Open the show view
    visit "/posts/#{@my_post.id}"
  end # before

  subject { page }

  it { should have_selector('h1', text: @my_post.title) }
  it { should have_content(@text_html) }

  it "should have a link to the source"
  it "should list the author"
end # describe
