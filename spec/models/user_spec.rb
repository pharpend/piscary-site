require 'spec_helper'

def check_for_appropriate_responses
  it { should respond_to(:username) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }
  it { should respond_to(:password_digest) }
end

def new_user(args)
  User.new(Hash[args])
end

def valid?
  check_for_appropriate_responses
  it { should be_valid }
end

def invalid?
  check_for_appropriate_responses
  it { should_not be_valid }
end

describe User do
  # pending "add some examples to (or delete) #{__FILE__}"

  before :each do
    @nuargs =  {
      username: "princess",
      password: "foobarbaz",
      password_confirmation: "foobarbaz",
      email: "htrain9000@gmail.com"
    }
  end

  subject { new_user(@nuargs) }

  describe "When everything is clean and serene" do
    valid?
  end # clean and serene

  describe "When the username is screwed up" do
    before { @nuargs[:username] = "f" }
    invalid?
 end

  describe "When the password is not of appropriate length" do
    before do
      @nuargs[:password] = @nuargs[:password_confirmation] = "foobar"
    end
    invalid?
  end

  describe "When the user doesn't have an email" do
    before do
      @nuargs.except! :email
    end
    valid?
  end

  describe "When the passwords don't match" do
    before do
      @nuargs[:password] = "perflipisclup"
    end
    invalid?
  end
end # User
