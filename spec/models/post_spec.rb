require 'spec_helper'

describe Post do
  before do
    @params = {
      title: "This is my title.",
      text: "This is my **text**.",
    }
    @tp = Post.new(@params)
  end # before

  subject { @tp }

  it { should respond_to :title }
  it { should respond_to :text }
  it { should respond_to :text_html }
  it { should be_valid }

  it "Should reject the post if the title is too short" do
    newparams = @params.update( { title: "" } )
    newsubject = Post.new(newparams)
    expect(newsubject).to_not be_valid
  end

  it "Should reject the post if the title is too long" do
    newparams = @params.update( { title: "arstarst"*3000 } )
    newsubject = Post.new(newparams)
    expect(newsubject).to_not be_valid
  end
end # Post
